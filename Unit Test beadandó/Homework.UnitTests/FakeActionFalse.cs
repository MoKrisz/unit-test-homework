﻿using Homework.ThirdParty;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homework.UnitTests
{
    class FakeActionFalse : IAction
    {
        public bool Execute()
        {
            return false;
        }
    }
}
