﻿using Homework.ThirdParty;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homework.UnitTests
{
    [TestFixture]
    class AccountActivityServiceTests
    {
        private AccountActivityService accountactiveservice;
        private Mock<IAccountRepository> mockAccountRepo;

        [SetUp]
        public void SetUp()
        {
            mockAccountRepo = new Mock<IAccountRepository>();
            accountactiveservice = new AccountActivityService(mockAccountRepo.Object);
        }

        [Test]
        public void GetActivity_Throws_Exception_IfAccountIsNull()
        {
            mockAccountRepo.Setup(x => x.Get(It.IsAny<int>())).Returns((Account)null);

            Assert.That(() => accountactiveservice.GetActivity(It.IsAny<int>()), Throws.TypeOf<AccountNotExistsException>());
        }

        [Test]
        public void GetActivity_Doesnt_Throw_Exception_IfAccountIsNotNull()
        {
            mockAccountRepo.Setup(x => x.Get(It.IsAny<int>())).Returns(new Account(It.IsAny<int>()));

            Assert.That(() => accountactiveservice.GetActivity(It.IsAny<int>()), Throws.Nothing);
        }

        [TestCase(ActivityLevel.None, 0)]
        [TestCase(ActivityLevel.Low, 1)]
        [TestCase(ActivityLevel.Low, 19)]
        [TestCase(ActivityLevel.Medium, 20)]
        [TestCase(ActivityLevel.Medium, 39)]
        [TestCase(ActivityLevel.High, 40)]
        [TestCase(ActivityLevel.High, 1000)]
        public void GetActivity_ActivityLevel_Depends_on_Activity(ActivityLevel level, int activityNum)
        {
            Mock<IAccount> accmock = new Mock<IAccount>();
            accmock.SetupGet(acc => acc.ActionsSuccessfullyPerformed).Returns(activityNum);
            mockAccountRepo.Setup(x => x.Get(It.IsAny<int>())).Returns(new Account(It.IsAny<int>()).InterfaceConverter(accmock.Object));

            ActivityLevel activity = accountactiveservice.GetActivity(It.IsAny<int>());

            Assert.That(accmock.Object.ActionsSuccessfullyPerformed, Is.Not.Null);
            Assert.That(activity, Is.EqualTo(level));
        }

        [TestCase(ActivityLevel.Low, 4)]
        [TestCase(ActivityLevel.Medium, 5)]
        [TestCase(ActivityLevel.High, 3)]
        [TestCase(ActivityLevel.None, 0)]
        public void GetAmountForActivity_Counts_ActivityLevels(ActivityLevel al, int count)
        {
            Mock<IAccount> iacc = new Mock<IAccount>();
            var accounts = new[]
            {
                new Account(1).InterfaceConverter(iacc.SetupProperty(acc => acc.ActionsSuccessfullyPerformed, 10).Object),
                new Account(2).InterfaceConverter(iacc.SetupProperty(acc => acc.ActionsSuccessfullyPerformed, 21).Object),
                new Account(3).InterfaceConverter(iacc.SetupProperty(acc => acc.ActionsSuccessfullyPerformed, 23).Object),
                new Account(4).InterfaceConverter(iacc.SetupProperty(acc => acc.ActionsSuccessfullyPerformed, 24).Object),
                new Account(5).InterfaceConverter(iacc.SetupProperty(acc => acc.ActionsSuccessfullyPerformed, 25).Object),
                new Account(6).InterfaceConverter(iacc.SetupProperty(acc => acc.ActionsSuccessfullyPerformed, 19).Object),
                new Account(7).InterfaceConverter(iacc.SetupProperty(acc => acc.ActionsSuccessfullyPerformed, 18).Object),
                new Account(8).InterfaceConverter(iacc.SetupProperty(acc => acc.ActionsSuccessfullyPerformed, 42).Object),
                new Account(9).InterfaceConverter(iacc.SetupProperty(acc => acc.ActionsSuccessfullyPerformed, 41).Object),
                new Account(10).InterfaceConverter(iacc.SetupProperty(acc => acc.ActionsSuccessfullyPerformed, 125).Object),
                new Account(11).InterfaceConverter(iacc.SetupProperty(acc => acc.ActionsSuccessfullyPerformed, 2).Object),
                new Account(12).InterfaceConverter(iacc.SetupProperty(acc => acc.ActionsSuccessfullyPerformed, 31).Object),
            };
            mockAccountRepo.Setup(x => x.GetAll()).Returns(accounts);
            accounts.ToList().ForEach(acc => mockAccountRepo.Setup(x => x.Get(acc.Id)).Returns(acc));

            int level = accountactiveservice.GetAmountForActivity(al);

            Assert.That(level, Is.EqualTo(count));
        }

        [Test]
        public void GetAmountForActivity_AccountRepoMethods_AreCalled_ExactTimes()
        {
            Mock<IAccount> iacc = new Mock<IAccount>();
            var accounts = new[]
            {
                new Account(1).InterfaceConverter(iacc.SetupProperty(acc => acc.ActionsSuccessfullyPerformed, 10).Object),
                new Account(2).InterfaceConverter(iacc.SetupProperty(acc => acc.ActionsSuccessfullyPerformed, 21).Object),
                new Account(3).InterfaceConverter(iacc.SetupProperty(acc => acc.ActionsSuccessfullyPerformed, 23).Object),
                new Account(4).InterfaceConverter(iacc.SetupProperty(acc => acc.ActionsSuccessfullyPerformed, 24).Object),
                new Account(5).InterfaceConverter(iacc.SetupProperty(acc => acc.ActionsSuccessfullyPerformed, 25).Object),
                new Account(6).InterfaceConverter(iacc.SetupProperty(acc => acc.ActionsSuccessfullyPerformed, 19).Object),
                new Account(7).InterfaceConverter(iacc.SetupProperty(acc => acc.ActionsSuccessfullyPerformed, 18).Object),
                new Account(8).InterfaceConverter(iacc.SetupProperty(acc => acc.ActionsSuccessfullyPerformed, 42).Object),
                new Account(9).InterfaceConverter(iacc.SetupProperty(acc => acc.ActionsSuccessfullyPerformed, 41).Object),
                new Account(10).InterfaceConverter(iacc.SetupProperty(acc => acc.ActionsSuccessfullyPerformed, 125).Object),
                new Account(11).InterfaceConverter(iacc.SetupProperty(acc => acc.ActionsSuccessfullyPerformed, 2).Object),
                new Account(12).InterfaceConverter(iacc.SetupProperty(acc => acc.ActionsSuccessfullyPerformed, 31).Object),
            };
            mockAccountRepo.Setup(x => x.GetAll()).Returns(accounts);
            accounts.ToList().ForEach(acc => mockAccountRepo.Setup(x => x.Get(acc.Id)).Returns(acc));

            int value = accountactiveservice.GetAmountForActivity(ActivityLevel.None);

            mockAccountRepo.Verify(x => x.GetAll(), Times.Once);
            mockAccountRepo.Verify(x => x.Get(It.IsAny<int>()), Times.Exactly(accounts.Length));
            mockAccountRepo.Verify(x => x.Get(10));
            mockAccountRepo.VerifyNoOtherCalls();
        }

        #region OlderTests
        //[Test]
        //public void GetActivity_ActivityLevel_IsNone_IfThereWasNoActivity()
        //{
        //    Account returnable = new Account(It.IsAny<int>());
        //    returnable.Register();
        //    mockAccountRepo.Setup(x => x.Get(It.IsAny<int>())).Returns(returnable);


        //    returnable.TakeAction(new FakeActionFalse());
        //    ActivityLevel activity = accountactiveservice.GetActivity(It.IsAny<int>());

        //    Assert.That(activity, Is.EqualTo(ActivityLevel.None));
        //}

        //[Test]
        //public void GetActivity_ActivityLevel_IsLow_IfActivityIsLesserThanTwenty()
        //{
        //    Mock<IAccount> accmock = new Mock<IAccount>();
        //    //accmock.SetupGet(acc => acc.ActionsSuccessfullyPerformed).Returns(It.IsInRange(20, 39, Moq.Range.Inclusive));
        //    accmock.SetupGet(acc => acc.ActionsSuccessfullyPerformed).Returns(19);
        //    mockAccountRepo.Setup(x => x.Get(It.IsAny<int>())).Returns(new Account(It.IsAny<int>()).InterfaceConverter(accmock.Object));

        //    ActivityLevel activity = accountactiveservice.GetActivity(It.IsAny<int>());

        //    Assert.That(accmock.Object.ActionsSuccessfullyPerformed, Is.Not.Null);
        //    Assert.That(activity, Is.EqualTo(ActivityLevel.Low));
        //}

        //[Test]
        //public void GetActivity_ActivityLevel_IsMedium_IfActivityIsLesserThanFourty()
        //{
        //    Mock<IAccount> accmock = new Mock<IAccount>();
        //    accmock.SetupGet(acc => acc.ActionsSuccessfullyPerformed).Returns(39);
        //    mockAccountRepo.Setup(x => x.Get(It.IsAny<int>())).Returns(new Account(It.IsAny<int>()).InterfaceConverter(accmock.Object));

        //    ActivityLevel activity = accountactiveservice.GetActivity(It.IsAny<int>());

        //    Assert.That(activity, Is.EqualTo(ActivityLevel.Medium));
        //}

        //[Test]
        //public void GetActivity_ActivityLevel_IsHigh_IfActivityIsGreaterThanFourty()
        //{
        //    Mock<IAccount> accmock = new Mock<IAccount>();
        //    accmock.SetupGet(acc => acc.ActionsSuccessfullyPerformed).Returns(50);
        //    mockAccountRepo.Setup(x => x.Get(It.IsAny<int>())).Returns(new Account(It.IsAny<int>()).InterfaceConverter(accmock.Object));

        //    ActivityLevel activity = accountactiveservice.GetActivity(It.IsAny<int>());

        //    Assert.That(activity, Is.EqualTo(ActivityLevel.High));
        //}
        #endregion
    }
}
