﻿using Homework.ThirdParty;
using Moq;
using NUnit.Framework;
using System;

namespace Homework.UnitTests
{
    [TestFixture]
    public class AccountTests
    {
        private Account account;
        private FakeAction actiontrue;
        private FakeActionFalse actionfalse;

        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            actiontrue = new FakeAction();
            actionfalse = new FakeActionFalse();
        }

        [Test]
        public void Account_Constructor_Sets_Id()
        {
            int id = 15;

            account = new Account(id);

            Assert.That(account.Id, Is.EqualTo(id));
        }

        [Test]
        public void Account_Register_IsTrue_IfSet()
        {
            account = new Account(10);

            account.Register();

            Assert.That(account.IsRegistered, Is.True);
        }

        [Test]
        public void Account_Register_IsFalse_IfNotSet()
        {
            account = new Account(10);

            Assert.That(account.IsRegistered, Is.False);
        }

        [Test]
        public void Account_Activate_IsTrue_IfSet()
        {
            account = new Account(10);

            account.Activate();

            Assert.That(account.IsConfirmed, Is.True);
        }

        [Test]
        public void Account_Activate_IsFalse_IfNotSet()
        {
            account = new Account(10);

            Assert.That(account.IsConfirmed, Is.False);
        }

        [Test]
        public void TakeAction_Throws_Exception_IfNotRegistered_And_IfNotActivated()
        {
            account = new Account(10);

            Assert.That(() => account.TakeAction(actiontrue), Throws.TypeOf<InactiveUserException>());
        }

        [Test]
        public void TakeAction_Throws_Nothing_IfRegistered_And_IfActivated()
        {
            account = new Account(10);
            account.Register();
            account.Activate();

            var value = account.TakeAction(actiontrue);

            Assert.That(() => account.TakeAction(actiontrue), Throws.Nothing);
        }

        [Test]
        public void TakeAction_Throws_Nothing_IfRegistered_And_IfNotActivated()
        {
            account = new Account(10);
            account.Register();

            var value = account.TakeAction(actiontrue);

            Assert.That(() => account.TakeAction(actiontrue), Throws.Nothing);
        }

        [Test]
        public void TakeAction_Throws_Nothing_IfNotRegistered_And_IfActivated()
        {
            account = new Account(10);
            account.Activate();

            var value = account.TakeAction(actionfalse);

            Assert.That(() => account.TakeAction(actiontrue), Throws.Nothing);
        }

        [Test]
        public void TakeAction_IsTrue_IfActionIsTrue()
        {
            account = new Account(10);
            account.Register();
            account.Activate();

            var value = account.TakeAction(actiontrue);

            Assert.That(value, Is.True);
        }

        [Test]
        public void TakeAction_IsFalse_IfActionIsFalse()
        {
            account = new Account(10);
            account.Register();
            account.Activate();

            var value = account.TakeAction(actionfalse);

            Assert.That(value, Is.False);
        }

        [Test]
        public void ActionsSuccessfullyPerformed_IsIncremented_IfActionIsTrue()
        {
            account = new Account(10);
            account.Register();
            account.Activate();

            var value = account.TakeAction(actiontrue);

            Assert.That(account.ActionsSuccessfullyPerformed, Is.GreaterThan(0));
        }

        [Test]
        public void ActionsSuccessfullyPerformed_IsNotIncremented_IfActionIsFalse()
        {
            account = new Account(10);
            account.Register();
            account.Activate();

            var value = account.TakeAction(actionfalse);

            Assert.That(account.ActionsSuccessfullyPerformed, Is.Zero);
        }

        //[Test]
        //public void Action_Calls_Properties()
        //{
        //    Mock<Account> acc = new Mock<Account>(It.IsAny<int>());
        //    acc.Object.Register();
        //    acc.Object.Activate();

        //    var value = acc.Object.TakeAction(actionfalse);

        //    acc.VerifyGet(x => x.IsRegistered, Times.Once);
        //    acc.VerifyGet(x => x.IsConfirmed, Times.Once);
        //}
    }
}
