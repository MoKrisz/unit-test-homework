﻿using Homework.ThirdParty;
using System;
using System.Collections.Generic;
using System.Text;

namespace Homework
{
    public interface IAccount
    {
		bool IsRegistered { get; }
		bool IsConfirmed { get; }

		int ActionsSuccessfullyPerformed { get; set; }

		bool TakeAction(IAction action);

		void ValidateAccount();

		bool IsInactiveAccount();
	}
}
